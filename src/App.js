import React from 'react';
import Header from './Header';
import {Footer} from './Footer';

class App extends React.Component {
  render() {
    let title = "React Website";
    return (
      <div>
        <Header websiteTitle={title} subtitle="some subtitle blah blah blah..." />
        <Footer />
      </div>
    );
  }
}

export default App;
